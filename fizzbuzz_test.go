package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFizzbuzz(t *testing.T) {
	tests := []struct {
		input  int
		result string
		err    error
	}{
		{input: 1, result: "1", err: nil},
		{input: 2, result: "2", err: nil},
		{input: 3, result: "Fizz", err: nil},
		{input: 5, result: "Buzz", err: nil},
		{input: 15, result: "FizzBuzz", err: nil},
		{input: -1, result: "", err: fmt.Errorf("[fizzbuzz] Invalid value -- input must be >= 0")},
	}

	assert := assert.New(t)
	for _, test := range tests {
		result, err := FizzbuzzPredicate(test.input)
		assert.IsType(err, test.err)
		assert.Equal(result, test.result)
	}
}
