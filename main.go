package main

import (
	"fmt"
	"strconv"
)

func isDivisibleBy(dividend int, divisor int) bool {
	return dividend%divisor == 0
}

// FizzbuzzPredicate is a pure function that accepts a number and returns:
// - "Fizz" if the number is a multiple of 3;
// - "Buzz" if the number is a multiple of 5;
// - "FizzBuzz" if the number is a multiple of both 3 and 5;
// - else the number simply cast to a string.
func FizzbuzzPredicate(value int) (string, error) {
	if value < 0 {
		return "", fmt.Errorf("[fizzbuzz] Invalid value -- input must be >= 0")
	}

	response := ""
	isDivisbleBy3 := isDivisibleBy(value, 3)
	isDivisbleBy5 := isDivisibleBy(value, 5)
	if isDivisbleBy3 {
		response += "Fizz"
	}
	if isDivisbleBy5 {
		response += "Buzz"
	}
	if !isDivisbleBy3 && !isDivisbleBy5 {
		response += strconv.Itoa(value)
	}
	return response, nil
}

func main() {
	fmt.Println("=============")
	fmt.Println("Hello Urbint!")
	fmt.Println("Give this code a spin in your browser: https://jstu.art/pklD")
	fmt.Println("=============")
	for i := 1; i <= 100; i++ {
		// Life's great questions.
		doesItFizzbuzz, _ := FizzbuzzPredicate(i)
		fmt.Println(doesItFizzbuzz)
	}
}
